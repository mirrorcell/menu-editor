Template.extraModalLine.events({
    'click #remove-extra-line': function() {
        var grp = MenuEditor.extraGroup.get();
        grp.options.splice(this.index, 1);
        MenuEditor.extraGroup.set(grp);
    },
    'keypress .extras-grp-name': function(e, t) {
        setTimeout(function() {
            var grp = MenuEditor.extraGroup.get();
            grp.name = $(e.currentTarget).val();
            MenuEditor.extraGroup.set(grp)
        }, 100);
    },
    'keypress .extra-line-name': function(e) {
        var self = this;
        setTimeout(function() {
            var grp = MenuEditor.extraGroup.get();
            console.log(self);
            grp.options[self.index].name = $(e.currentTarget).val();
            MenuEditor.extraGroup.set(grp)
        }, 100);
    },
    'keypress .extra-line-price': function(e) {
        var self = this;
        setTimeout(function() {
            var grp = MenuEditor.extraGroup.get();
            grp.options[self.index].price = $(e.currentTarget).val();
            MenuEditor.extraGroup.set(grp)
        }, 100);
    }
});

Template.addModalLine.events({
    'click #add-extra-line': function() {
        console.log("adding new line!");
        var grp = MenuEditor.extraGroup.get();
        grp.options.push({ name: '', price: 0.00});
        MenuEditor.extraGroup.set(grp);
    }
});

Template.extraModal.helpers({
    'existingExtras': function() {
        return MenuEditor.extraGroup.get();
    }
});