Router.route('/partners/menu/edit/:_id', {
    name:   'menuEditor',
    data:   function () {

        console.log("PARTNERS MENU EDIT", this.params._id);
        var menu = FB_Core.Menus.findOne({
            partnerId: this.params._id
        });
        var partner = FB_Core.Partners.findOne({
            _id: this.params._id
        });
        console.log("Returning with: " + menu + " for: " + this.params.id);
        return {
            menu:    menu,
            partner: partner
        };
    },
    action: function () {
        this.render('menuEditor');
    }
});