$.fn.form.settings.rules["currency"] = function(value) {
    var regex = new RegExp('[0-9]+(\.[0-9][0-9]?)?');
    console.log(regex.test(value));
    return regex.test(value);
};

var tag = '[MenuEditor] ';
//PartnerMenuItems.initEasySearch('name');
//FB_Core.PartnerMenuItemExtras.initEasySearch('extra.name');

var partner;

var currentSelectedCategory = new ReactiveVar();

Template.menuEditor.onRendered(function() {
    partId = Router.current().params._id;

    this.subscribe('partnerMenuItemExtras', { partnerId : partId });
    this.subscribe('partnerMenu', { partnerId : partId }, function() {
        var menus = FB_Core.Menus.find().fetch();

        if(MenuEditor.menus.length !== 0)
            return;

        console.log("FOUND PARTNER MENU?" , menus);
        if(menus.length === 0) {

        } else {
            console.log("FOUND MENU" + menus);
            _.each(menus, function(menu) {
                MenuEditor.menus.push(menu);
            });
            MenuEditor.selectedMenu.set({name : MenuEditor.menus[0].name, i : 0});
        }
        MenuEditor.changed();
        MenuEditor.hasChanged.set(false);
    });

    $('.ui.radio.checkbox').checkbox();
});

Template.menuEditor.helpers({
    hasChanged: function() {
        return MenuEditor.hasChanged.get();
    }
});

Template.menuEditor.events({
    'click #save-footer': function() {
        MenuEditor.saveMenu(function() {
            MenuEditor.hasChanged.set(false);
        });
    }
});

Template.menuDisplay.helpers({
    menus: function() {
        MenuEditor.depend();
        return MenuEditor.menus;
    },
    firstActive: function() {
        var selectedMenu = MenuEditor.selectedMenu.get();

        console.log('menu ' + this.value.name);
        if(selectedMenu.name === this.value.name) {
            return 'active';
        } else {
            console.log("DONT RETURN");
            return '';
        }
    },
    partner: function() {
        return FB_Core.Partners.findOne(Router.current().params._id);
    }
});

Template.menuDisplay.events({
    //'mouseenter .category-menu-item': function(e) {
    //    var target = $('#menu-item-buttons-'+this.parentIndex+'-'+this.index);
    //    console.log(target);
    //    target.removeClass('hide-field');
    //},
    //'mouseleave .category-menu-item': function(e) {
    //    var target = $('#menu-item-buttons-'+this.parentIndex+'-'+this.index);
    //    $(target).addClass('hide-field');
    //},
    'click #add-menu-tab-button': function(e) {
        NotificationController.promptWithInput("Enter A Name", "New Menu", function(name) {
            if(name) {
                MenuEditor.addMenu(name);
            }
        });
    },
    'click .add-menu-category': function(e) {
        NotificationController.promptWithInput("Enter A Name", "New Category", function(name) {
            if(name) {
                MenuEditor.addCategoryToMenu(name);
            }
        });
    },
    'click .remove-menu-category': function(e) {
        NotificationController.confirm("Are you want to delete this category?", function(res) {
            res && MenuEditor.removeCategoryFromMenu(this.index);
        });
    },
    'click .menu-tab' : function(e) {
        console.log("Clicked menu tab", this);
        MenuEditor.selectedMenu.set({name : this.value.name, i : this.index});
    },
    'click .remove-category-item': function(e, t){
        var index = this.index;
        var parent = this.parentIndex;

        console.log("Remove clicked", index, parent);

        if(_.isNumber(index) && _.isNumber(parent)) {
            NotificationController.confirm("Are You Sure You Want To Delete This Item?", function(res) {
                if(res) {
                    MenuEditor.removeItem(parent, index);
                }
            });
        }
    },
    'click .remove-menu' : function() {
        var menu = MenuEditor.getSelectedMenu();
        if(menu) {
            NotificationController.confirm("Are You Sure You Want To Delete This Menu?", function (res) {
                if (res) {
                    MenuEditor.removeMenu(menu.i);
                }
            });
        }
    }
});

Template.menuCategory.events({
    'click .add-category-item': function(e) {
        console.log("Adding item to category", this.index);
        MenuEditor.addItemToCategory(this.index);
    },
    'click .edit-category': function(e) {
        var self = this;
        NotificationController.promptWithInput("Enter A Name", "Edit Category Name", function(name) {
            MenuEditor.editCategoryName(name, self.index);
        });
    },
    'click .edit-category-item' : function(e) {
        console.log("THIS", this);
        MenuEditor.editItem(this.parentIndex, this.index);
    }
 });


Template.menuEditor.events({
    'click .add-item-to-category': function(e) {
        e.stopPropagation();
        var cop = _.clone(this);
        delete cop._id;
        cop.id = uuid.v1();
        MenuEditor.menu.categories[currentSelectedCategory.get() - 1].items.push(cop);

        MenuEditor.changed();
    },
    'click #save-menu-button' : function() {
        MenuEditor.saveMenu();
    },
    'click #remove-menu-item-button': function(e) {
        e.stopPropagation();
        console.log("Removing");
        MenuEditor.removeItem(this.parentIndex, this.index);
    },
    'click #menu-category-header': function(e) {
        e.stopPropagation();
        console.log("Accordianing header");
        var el = '#menu-cat-item-list-'+this.index;
        if($(el).is(":visible")) {
            $(el).hide();
        } else {
            $(el).show();
        }
    }

});

getCategoryItem = function(catIndex, index) {
    return MenuEditor.menu.categories[catIndex].items[index];
};