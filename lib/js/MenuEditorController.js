function clearItemModalFields() {
    var name = $("#new-item-name").val(''),
        price = $('#new-item-price').val(''),
        desc = $('#new-item-desc').val(''),
        tic = $('input[name=food-type]:checked').data().tic;
}

function resetItemVars() {
    MenuEditor.extraModeNew.set(false);
    MenuEditor.newExtra.set(false);
    MenuEditor.extraGroup.set(null);
    clearSelectedOptions();
}

function resetItemModal() {
    clearItemModalFields();
    resetItemVars();
}

function createSelectedOptions() {
    MenuEditor.currentSelectedOptions.set({ extras: [{name :'', price: ''}] });
}

function clearSelectedOptions() {
    MenuEditor.currentSelectedOptions.set(null);
}

function setSelectedOptions(extras) {
    console.log("Settings selected options", extras);
    MenuEditor.currentSelectedOptions.set({extras : extras});
}

getSelectedOptions = function() {
    var current = MenuEditor.currentSelectedOptions.get();
    return current && current.extras;
};

function validateItemModal(success, failure) {
    var rules = {
        name: {
            identifier: 'new-item-name',
            rules: [{
                type: 'empty',
                prompt: 'Please Enter A Name'
            }
            ]
        },
        price: {
            identifier: 'new-item-price',
            rules: [{
                type: 'currency',
                prompt: 'Please Enter A Valid Dollar Amount'
            }]
        },
        description: {
            identifier: 'new-item-desc',
            rules: [{
                type: 'empty',
                prompt: 'Please Enter A Description'
            }]
        }
    };
    var settings = {
        inline : true,
        onSuccess: function() {
            console.log("Form validation success");
            success && success();
        },
        onFailure: function() {
            console.log("Form validation not success");
            failure && failure();
        }
    };

    $("#item-form").form(rules, settings);
    $("#item-form").form('validate form');
}

function validateExtrasModal(success, failure) {
    var rules = {
        grpName: {
            identifier: 'extras_grp_name',
            rules: [{
                type: 'empty',
                prompt: 'Please Enter a Extras Name'
            }]
        },
        price: {
            identifier: 'extra_line_price',
            rules: [{
                type: 'currency',
                prompt: 'Please Enter A Valid Dollar Amount'
            }]
        },
        lineName: {
            identifier: 'extra_line_name',
            rules: [{
                type: 'empty',
                prompt: 'Please Enter a Extras Name'
            }]
        }

    };
    var settings = {
        inline : true,
        onSuccess: function() {
            console.log("Form validation success");
            success && success();
        },
        onFailure: function() {
            console.log("Form validation not success");
            failure && failure();
        }
    };

    $("#extra-modal-form").form(rules, settings);
    $("#extra-modal-form").form('validate form');
}

MenuEditor = {
    dependency : new Deps.Dependency(),
    menus: [],
    removedMenuIds: [],
    selectedMenu: new ReactiveVar(null),
    selectedCategory: null,
    newExtra : new ReactiveVar(false),
    extraModeNew : new ReactiveVar(false),
    extraGroup : new ReactiveVar(null),
    hasExtras : new ReactiveVar(false),
    extrasEnabled : new ReactiveVar(false),
    currentSelectedOptions : new ReactiveVar(null),
    currentSelectedItem : new ReactiveVar({}),
    hasChanged : new ReactiveVar(false),
    itemModalType : new ReactiveVar('Add'),
    categoryAccordian: [],
    getCurrentItem: function() {
        return this.currentSelectedItem.get();
    },
    setCurrentItem: function(item, parentIndex, index) {
        console.log("Setting current item", item, parentIndex, index);
        if(_.isNumber(parentIndex) && _.isNumber(index)) {
            this.currentSelectedItem.set({ parentIndex: parentIndex, index: index, item: item });
        } else {
            var currentItem = this.getCurrentItem();
            if(_.isNumber(currentItem.parentIndex) && _.isNumber(currentItem.index)) {
                console.log("Current item had last parent index but none were passed, get and set");
                this.currentSelectedItem.set({ item: item, parentIndex: currentItem.parentIndex, index: currentItem.index });
            } else {
                this.currentSelectedItem.set({item: item});
            }
        }
    },
    clearCurrentItem: function() {
        this.currentSelectedItem.set({});
    },
    popNewItem: function() {
        var item = {
            name: '',
            price: 0,
            description : '',
            extras: []
        };
        this.setCurrentItem(item);
    },
    addMenu: function(name) {
        if(name) {
            console.log("Adding New Menu", name);
            this.menus.push({partnerId : partId, name: name, categories: []});
            this.changed();
        }
    },
    addItemToCategory: function(categoryIndex) {
        var self = this;
        var currentMenu = this.getSelectedMenu();
        this.popNewItem();

        this.openItemModal({ modalType: 'add'}, function(item) {
            console.log("Validation success");
            var name = $("#new-item-name").val(),
                price = $('#new-item-price').val(),
                desc = $('#new-item-desc').val(),
                tic = $('input[name=food-type]:checked').data().tic;


            var item = self.getCurrentItem().item;
            item.name = name;
            item.price = price;
            item.description = desc;
            item.tic = tic;

            console.log("Inserting", item);
            self.menus[currentMenu.i].categories[categoryIndex].items.push(item);

            self.setCurrentItem(item);


            self.changed();
            $('#new-item-modal').modal('hide');

            resetItemModal();
        });

    },
    editItem: function(parentIndex, index) {
        var self = this;
        var currentMenu = this.getSelectedMenu();
        var item = this.menus[currentMenu.i].categories[parentIndex].items[index];



        this.openItemModal({ modalType: 'edit', item: item, parentIndex: parentIndex, index: index }, function() {
            var update = {
                name: $("#new-item-name").val(),
                price: $('#new-item-price').val(),
                description: $('#new-item-desc').val(),
                tic: $('input[name=food-type]:checked').data().tic
            };

            var item = self.getCurrentItem().item;
            if(item && item.extras && item.extras.length > 0) {
                update.extras = item.extras;
            }

            console.log("Updating", update);


            self.menus[currentMenu.i].categories[parentIndex].items[index] = update;
            MenuEditor.changed();
            $('#new-item-modal').modal('hide');
            resetItemModal();

        });
    },
    addCategoryToMenu: function(name) {
        var currentMenu = this.selectedMenu.get();
        this.menus[currentMenu.i].categories.push({name : name, items: []});
        this.changed();
    },
    removeCategoryFromMenu: function(index) {
        var currentMenu = this.getSelectedMenu();
        this.menus[currentMenu.i].categories.splice(index, 1);
        this.changed();
    },
    editCategoryName: function(name, index) {
        var currentMenu = this.getSelectedMenu();
        console.log(this.menus[currentMenu.i]);
        console.log(this.menus[currentMenu.i].categories[index]);
        this.menus[currentMenu.i].categories[index].name = name;
        this.changed();
    },
    clearExtraGroup: function() {
        $("#extras-grp-name").val('');
        this.extraGroup.set(null);
    },
    saveItem: function(parentIndex, index, item) {
        this.menu.categories[parentIndex].items[index] = item;
    },
    removeItem: function(parentIndex, index) {
        var menu = this.getSelectedMenu();
        console.log("Trying to remove", parentIndex, index);
        this.menus[menu.i].categories[parentIndex].items.splice(index, 1);
        this.changed();
    },
    getSelectedMenu: function() {
        return this.selectedMenu.get();
    },
    removeMenu: function(menuIndex) {
        var thisId = this.menus[menuIndex]._id;

        this.removedMenuIds.push(thisId);
        this.menus.splice(menuIndex, 1);
        this.changed();
    },
    removeExtraFromItem: function(index) {
        var extras = getSelectedOptions();
        console.log(extras);
        extras.splice(index, 1);
        console.log('sliced', extras);
        setSelectedOptions(extras);
    },
    saveMenu: function(callback) {
        var menus = this.menus;
        var dbMenus = FB_Core.Menus.find().fetch();

        if(this.removedMenuIds.length > 0) {
            for(var i = 0; this.removedMenuIds.length > i; i++) {
                FB_Core.Menus.remove(this.removedMenuIds[i]);
                this.removedMenuIds.splice(i, 1);
            }
        }

        _.each(menus, function(menu) {
           //Update
            console.log("UPDATING MENU", menu, menu._id);
            if(menu._id) {
                var id = menu._id;
                FB_Core.Menus.update(
                    menu._id,
                    { $set : menu },
                    function() {
                        menu._id = id;
                        console.log("MENUmenu", menu);
                    });



            } else { // insert
                var newId = FB_Core.Menus.insert(menu);
                menu._id = newId;
            }
        });

        callback && callback();
    },
    openItemModal : function(options, successCB, failureCB) {
        console.log("Opening item modal with options:", options);
        var modalType = options.modalType || 'add',
            item = options.item,
            self = this,
            index,
            parentIndex;

        self.extrasEnabled.set(false);

        this.itemModalType.set(modalType);

        $('.ui.toggle.checkbox').checkbox({
            onChecked: function() {
                console.log("IS CHECKED");
                self.extrasEnabled.set(true);
            },
            onUnchecked: function() {
                self.extrasEnabled.set(false);
            }
        });

        if(item) {
            $("#new-item-name").val(item.name);
            $('#new-item-price').val(item.price);
            $('#new-item-desc').val(item.description);

            if(item.extras && item.extras.length > 0) {
                setSelectedOptions(item.extras);
                $('.ui.toggle.checkbox').checkbox('check');
            }

            index = options.index;
            parentIndex = options.parentIndex;

            MenuEditor.setCurrentItem(item, parentIndex, index);
        }

        setTimeout(function() {
            $('#new-item-modal')
                .modal({
                    closable: false,
                    allowMultiple: true,
                    onDeny: function () {
                        MenuEditor.clearCurrentItem();
                        failureCB && failureCB();
                    },
                    onApprove: function () {
                        validateItemModal(function () {
                            successCB && successCB(item);
                            MenuEditor.clearCurrentItem();
                        });
                        return false;
                    },
                    onHide: function () {
                        MenuEditor.clearCurrentItem();
                        failureCB && failureCB();
                        resetItemModal();
                    }
                })
                .modal('setting', 'transition', 'fade up')
                .modal('show');
        }, 100);
    },
    openExtrasModal: function(options, success, failure) {
        //load the extras into the modal
        var item = this.getCurrentItem(),
            self = this;

        if(options && options.extras) {

        } else {
            MenuEditor.extraGroup.set({ name : '', options: [{'name' : '', 'price': 0 }] });
        }

        setTimeout(function() {
            $('#extra-modal')
                .modal({
                    closable: false,
                    allowMultiple: true,
                    onDeny: function () {
                    },
                    onApprove: function () {
                        validateExtrasModal(function () {
                            if(item) {
                                self.addExtrasGroupToItem(item.parentIndex, item.index);
                            }
                            success && success();
                            $('#extra-modal').modal('hide');

                        });
                        return false;
                    },
                    onHide: function () {
                        failure && failure();

                    }
                })
                .modal('setting', 'transition', 'fade up')
                .modal('show');
        }, 100);
    },
    addExtrasGroup: function(extraGroup) {
        var partnerId = Router.current().params._id;
        return FB_Core.PartnerMenuItemExtras.insert({ partnerId: partnerId, extra: extraGroup });
    },
    addExtrasGroupToItem: function(extraOverload) {
        var extra;
        if(extraOverload) {
            extra = extraOverload;
        } else {
            extra = this.extraGroup.get();
            extra.name = $("#extras-grp-name").val();
        }
        var item = MenuEditor.getCurrentItem().item;
        if(!(item && item.extras)) {
            console.log("CURRENT ITEM DOESNT HAVE EXTRAS", item);
            item.extras = [];
        } else {
            console.log("CURRENT ITEM HAS EXTRAS DONT REMOVE");
        }

        console.log("PUSHING EXTRA", extra);
        item.extras.push(extra);
        this.addExtrasGroup(extra);

        console.log("SETTING ITEM", item);

        MenuEditor.setCurrentItem(item);
        this.clearExtraGroup();

    },
    changeItemExtra: function(parentIndex, index) {
        var extra = MenuEditor.extraGroup.get();
        MenuEditor.menus.categories[parentIndex].items[index].extras.push(extra)
    },
    depend: function() {
        return this.dependency.depend();
    },
    changed: function() {
        this.hasChanged.set(true);
        this.dependency.changed();
    }
};
