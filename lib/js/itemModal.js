Template.newItemModal.helpers({
    itemModalType: function() {
        return MenuEditor.itemModalType.get();
    }
});

//var enabled = new ReactiveVar(false);

Template.extrasArea.helpers({
    extrasEnabled: function() {
        return MenuEditor.extrasEnabled.get();
    },
    item: function() {
        var item =  MenuEditor.getCurrentItem();
        console.log("Returning", item);
        return item;
    },
    newExtra: function() {
        return MenuEditor.newExtra.get();
    }
});

Template.extrasArea.events({
    'click #add-extra-button': function() {
        MenuEditor.openExtrasModal();
    }
});

Template.extrasTypeAhead.helpers({
    existingExtras : function() {
        var partnersExtras = FB_Core.PartnerMenuItemExtras.find().fetch().map(function(object) {
            console.log("OBJECT", object && object.extra && object.extra.name);
            if(object && object.extra && object.extra.name)
                return { id: object._id, value: object.extra.name };
        });
        console.log("Returning d", partnersExtras);
        return partnersExtras;

    },
    selected: function(event, suggestion, datasetName) {
        // event - the jQuery event object
        // suggestion - the suggestion object
        // datasetName - the name of the dataset the suggestion belongs to
        // TODO your event handler here
        console.log("SELECTED HIT");
        console.log(suggestion.id);
        console.log(suggestion);

        var extra = FB_Core.PartnerMenuItemExtras.findOne(suggestion.id);

        if(extra) {
            MenuEditor.addExtrasGroupToItem(extra.extra);
        }
    }
});

Template.extrasTypeAhead.onRendered(function() {
    Meteor.typeahead.inject();
});
