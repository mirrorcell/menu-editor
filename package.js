Package.describe({
  name: 'menu-editor',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
    api.use(['iron:router', 'templating', 'reactive-var'], 'client');
    api.use("meteor-platform");

    var pathToJs = 'lib/js/';
    var pathToTemp = 'lib/templates/';

    var jsFiles = ['extraModal.js', 'itemModal.js', 'menuEditor.js', 'MenuEditorController.js'];
    for(var i = 0; i < jsFiles.length; i++) {
        jsFiles[i] = pathToJs + jsFiles[i];
    }
    var tempFiles = ['extraModal.html', 'itemModal.html', 'menuCategory.html', 'menuDisplay.html', 'menuEditor.html'];
    for(var i = 0; i < tempFiles.length; i++) {
        tempFiles[i] = pathToTemp + tempFiles[i];
    }

    var clientFiles = tempFiles.concat(jsFiles);

    api.versionsFrom('1.1.0.2');
    api.addFiles(clientFiles, 'client');
    api.addFiles(pathToJs + 'router.js', 'client');

    api.export('MenuEditor');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('menu-editor');
  api.addFiles('menu-editor-tests.js');
});
